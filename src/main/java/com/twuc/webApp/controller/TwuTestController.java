package com.twuc.webApp.controller;

import com.twuc.webApp.model.Message;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TwuTestController {
    @GetMapping("/api/no-return-value")
    void returnVoid(){}

    @GetMapping("/api/no-return-value-with-annotation")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    void returnStatusCode(){}

    @GetMapping("/api/messages/{message}")
    String returnStringMessage(@PathVariable("message") String message){
        return message;
    }

    @GetMapping("/api/message-objects/{message}")
    public Message getObjectMessage(@PathVariable String message) {
        return new Message(message);
    }

    @GetMapping("/api/message-objects-with-annotation/{message}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Message getMessageWithAnnotation(@PathVariable String message) {
        return new Message(message);
    }

    @GetMapping("/api/message-entities/{message}")
    public ResponseEntity<Message> messageResponseEntity(@PathVariable String message) {
        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .header("X-Auth", "me")
                .body(new Message(message));
    }

}
