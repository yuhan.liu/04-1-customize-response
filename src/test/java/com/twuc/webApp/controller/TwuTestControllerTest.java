package com.twuc.webApp.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@SpringBootTest
@AutoConfigureMockMvc
class TwuTestControllerTest {
    @Autowired
    MockMvc mockMvc;
    private ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        objectMapper = new ObjectMapper();
    }


    @Test
    void should_get_void() throws Exception {
        mockMvc.perform(get("/api/no-return-value"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    void should_return_204_in_use_ResponseStatus() throws Exception {
        mockMvc.perform(get("/api/no-return-value-with-annotation"))
                .andExpect(MockMvcResultMatchers.status().is(204));
    }

    @Test
    void should_return_String_message() throws Exception {
        mockMvc.perform(get("/api/messages/message"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("text/plain;charset=UTF-8"))
                .andExpect(MockMvcResultMatchers.content().string("message"));
    }

    @Test
    void should_return_class_message() throws Exception {
        mockMvc.perform(get("/api/message-objects/message"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.value").value("message"));
    }

    @Test
    void should_return_the_status_code_and_response() throws Exception {
        mockMvc.perform(get("/api/message-objects-with-annotation/message"))
                .andExpect(MockMvcResultMatchers.status().isAccepted())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.value").value("message"));
    }

    @Test
    void should_return_response_entity_with_use_responseEntity() throws Exception {
        mockMvc.perform(get("/api/message-entities/message"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.header().string("X-Auth", "me"))
                .andExpect(jsonPath("value").value("message"));
    }

}
